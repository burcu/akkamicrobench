import sys
from subprocess import run


def main(chainLength, seededBug):
    schedulers = ["RANDOM", "RAPOS", "POS", "DPOS", "PCTCP", "TaPCTCP"]

    initialSeed = 12345677
    depth = 3
    numMsgsToDependentActor = 3
    numIterations = 500

    numIndependentActors = [0, 1, 3, 9]

    for indActorIndex in range(len(numIndependentActors)):
        for schIndex in range(len(schedulers)):
            counter = 1
            while(counter < numIterations):
                seed = initialSeed + counter
                fileName = str(schedulers[schIndex] + "Alpha" + str(numIndependentActors[indActorIndex])+"D"+seededBug)
                folderName = "alpha"
                params = "{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10}" \
                    .format(counter, schedulers[schIndex], seed, depth, numMsgsToDependentActor, numIndependentActors[indActorIndex],
                            chainLength, chainLength, seededBug, fileName, folderName)
                #print(params)
                command = "java -jar target/scala-2.12/microbench-assembly-0.1.jar {0}".format(params)
                #print(command)
                run(command, shell=True)
                counter += 1
        chainLength = int(chainLength) + 1


if __name__ == '__main__':
    if len(sys.argv) == 3:
        chainLength = sys.argv[1]
        seededBug = sys.argv[2]
        main(chainLength, seededBug)
    else:
        print("No arguments for the chain length and seededBug.")
