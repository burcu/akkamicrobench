# Running for chain length (1 to 5) with d=2
time python3 scripts/runChainLength.py 1 5 2

# Running for varying chain length (2 to 6) with d=5
time python3 scripts/runChainLength.py 2 6 3

# Running for varying alpha (0, 0.25, 0.5, 0.75) with d=2
time python3 scripts/runAlpha.py 2 2

# Running for varying alpha (0, 0.25, 0.5, 0.75) with d=3
time python3 scripts/runAlpha.py 3 3