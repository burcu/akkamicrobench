import sys
from os import listdir
from os.path import isfile, join

Methods = ["RANDOM", "RAPOS", "POS", "DPOS", "PCTCP", "TaPCTCP"]
Alphas = [0, 1, 3, 9]
AlphaHeaders = ['0', '0.25', '0.5', '0.75']
ChainLengths = {
    2: [1, 2, 3, 4, 5],
    3: [2, 3, 4, 5, 6]
}


def header(d, col_values, col_prefix="", col_header=None):
    header = ["d = {0}".format(d)]
    for idx, c in enumerate(col_values):
        header.append(col_prefix + str(col_header[idx] if col_header else c))
    return '\t'.join(header)


def count_violations(f_name):
    with open(f_name, 'r') as file:
        lines = [line.rstrip('\n') for line in file if line.startswith('Detected violation')]
    return len(lines)


def print_table(table, d, headers, col_prefix='', col_header=None):
    print(header(d, headers, col_prefix=col_prefix, col_header=col_header))
    print('\t'.join(["-------"] * (len(headers) + 1)))
    for m in Methods:
        pl = [m]
        for h in headers:
            pl.append(str(table.get(m, {}).get(h, 0)))
        print('\t'.join(pl))


def process_directory(folder, name_limiter):
    tables = {}
    for f in listdir(folder):
        if isfile(join(folder, f)):
            params = f.split(name_limiter)
            if len(params) != 2:
                print("Invalid file: {0}".format(f))
                continue

            method, (p1, d) = params[0], params[1].split('D')

            v_c = count_violations(join(folder, f))
            d_table = tables.get(int(d), {})
            m_table = d_table.get(method, {})
            m_table[int(p1)] = v_c
            d_table[method] = m_table
            tables[int(d)] = d_table
    return tables


def make_alpha_tables(folder):
    tables = process_directory(folder, 'Alpha')
    print_table(tables[2], 2, Alphas, col_header=AlphaHeaders)
    print("")
    print_table(tables[3], 3, Alphas, col_header=AlphaHeaders)


def make_chain_tables(folder):
    tables = process_directory(folder, 'ChainLength')
    print_table(tables[2], 2, ChainLengths[2])
    print("")
    print_table(tables[3], 3, ChainLengths[3])


def main(folder):
    make_alpha_tables(folder)


if __name__ == '__main__':
    alphas_folder = './alpha'
    if len(sys.argv) == 2:
        alphas_folder = sys.argv[1]
    print("Processing results from: {0}".format(alphas_folder))
    main(alphas_folder)
