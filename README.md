Akka Microbenchmarks for Randomized Testing
=========================

This project implements simple Akka actor programs used for evaluating different randomized algorithms.

### Building and running an example app:

Requirements:

- Java 8 SDK
- Scala 2.12
- [Scala Build Tool](http://www.scala-sbt.org/) 



Build the project assembly:

```
sbt assembly
```


Run the example application with the following arguments, where scheduler is one of "PCTCP", "TAPCTCP", "POS", "DPOS" or "RANDOM".

```
java -jar target/scala-2.12/microbench-assembly-0.1.jar <testId> <scheduler> <seed> <depth> <numMsgsToDependentActor> <numIndependentActors> <chainSizeDependent> <chainSizeIndependent> <seededBugDepth> <outFileName> <outFolderName>
```

Example usage:

```
java -jar target/scala-2.12/microbench-assembly-0.1.jar 1 PCTCP 1234567 2 3 3 4 4 2 out.txt "out"
```
