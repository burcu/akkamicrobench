package bench

import java.io.{File, FileOutputStream, PrintWriter}

object Reporter {

  def write(s: String, file: String): Unit = {
    val logFile = new File(file)
    if (!logFile.getParentFile.exists()) {
      logFile.getParentFile.mkdirs()
    }

    val writer = new PrintWriter(new FileOutputStream(logFile, true))
    try {
      writer.append(s + "\n")
    }
    finally {
      writer.close()
    }
  }
}

