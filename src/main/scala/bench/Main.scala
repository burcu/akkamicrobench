package bench

import pctcp.{PCTCPOptions, TaPCTCPOptions}
import scheduler.pos.{DPOSOptions, POSOptions}
import scheduler.random.RandomWalkOptions
import scheduler.rapos.RaposOptions

object Main {

  def main(args: Array[String]): Unit = {
    if (args.length < 10) {
      println("No arguments for: testId, scheduler, seed, depth, numMsgsToDependentActor, numIndependentActors, chainLengthOfDependentActors, chainLengthOfInDependentActors, seededBugDepth, fileName and folder name")
      System.exit(-1)
    }

    val testId = args(0)
    val scheduler = args(1)
    val seed = args(2).toInt
    val depth = args(3).toInt
    val numMsgsToDependentActor = args(4).toInt
    val numIndependentActors = args(5).toInt
    val chainLengthOfDependentActors = args(6).toInt
    val chainLengthOfInDependentActors = args(7).toInt
    val seededBug = args(8).toInt
    val fileName = args(9)
    val folderName = args(10)

    val benchMarkOptions =  BenchMarkOptions(numMsgsToDependentActor, numIndependentActors, chainLengthOfDependentActors, chainLengthOfInDependentActors, seededBug, folderName + "/" + fileName)
    Reporter.write("Running test " + testId, folderName + "/" + fileName)

    val schedulerOptions = scheduler.toUpperCase match {
      case "PCTCP" => PCTCPOptions(randomSeed = seed, maxMessages = (numMsgsToDependentActor * chainLengthOfDependentActors) + (numIndependentActors * chainLengthOfInDependentActors), bugDepth = depth)
      case "TAPCTCP" => TaPCTCPOptions(randomSeed = seed, maxRacyMessages = numMsgsToDependentActor * chainLengthOfDependentActors, racyMessages = List("Racy"), bugDepth = depth)
      case "POS" => POSOptions(randomSeed = seed)
      case "DPOS" => DPOSOptions(randomSeed = seed, maxRacyMessages = numMsgsToDependentActor * chainLengthOfDependentActors, bugDepth = depth)
      case "RAPOS" => RaposOptions(randomSeed = seed)
      case "RANDOM" => RandomWalkOptions(randomSeed = seed)
      case _ => RandomWalkOptions(randomSeed = seed)
    }

    new Benchmark(benchMarkOptions, schedulerOptions).run
  }
}