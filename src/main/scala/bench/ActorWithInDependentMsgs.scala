package bench

import akka.actor.{Actor, ActorLogging, Props}

class ActorWithInDependentMsgs extends Actor with ActorLogging {

  import ActorWithInDependentMsgs._

  def receive: PartialFunction[Any, Unit] = {
    case Request(chainLength) =>
      //log.info("Received request: " + chainLength)
      if(chainLength-1 > 0) self ! Response(chainLength - 1)

    case Response(length) =>
      //log.info("Received self-response: " + length)
      if(length-1 > 0) self ! Response(length - 1)

    case _ => println("Unrecognized message")
  }
}

object ActorWithInDependentMsgs {
  val props: Props = Props[ActorWithInDependentMsgs]
  case class Request (msgCount: Int)
  case class Response(msgCount: Int)
}