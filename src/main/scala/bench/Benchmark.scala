package bench

import akka.actor.{ActorRef, ActorSystem}
import akka.dispatch.TestingDispatcher
import pctcp.{PCTCPOptions, TaPCTCPOptions}
import scheduler.SchedulerOptions
import scheduler.pos.{DPOSOptions, POSOptions}
import scheduler.random.RandomWalkOptions
import scheduler.rapos.RaposOptions

class Benchmark (options: BenchMarkOptions, schedulerOptions: SchedulerOptions) {
  def run = {
    import system.dispatcher

    val system = ActorSystem("sys")

    val actorWithDependentMsgs: ActorRef = system.actorOf(ActorWithDependentMsgs.props(options.chainLengthOfDependentActors, options.seededBug, options.fileName))
    val inDependentActors: Array[ActorRef] = new Array[ActorRef](options.numIndependentActors)
    (0 until options.numIndependentActors).foreach(i => inDependentActors(i) = system.actorOf(ActorWithInDependentMsgs.props))

    var msgId = 1
    if(options.chainLengthOfDependentActors != 0)
      (1 to options.numMsgsToDependentActor).foreach(_ => {
        actorWithDependentMsgs ! ActorWithDependentMsgs.RacyRequest(msgId, options.chainLengthOfDependentActors)
        msgId = msgId + options.chainLengthOfDependentActors
      })

    if(options.chainLengthOfInDependentActors != 0)
      (0 until options.numIndependentActors).foreach(i => inDependentActors(i) ! ActorWithInDependentMsgs.Request(options.chainLengthOfInDependentActors))

    if (dispatcher.isInstanceOf[TestingDispatcher]) {
      schedulerOptions match {
        case option: PCTCPOptions =>
          TestingDispatcher.setActorSystem(system)
          //println("Starting with options: " + schedulerOptions + " ----------------------------------------------")
          TestingDispatcher.setUp("PCTCP", schedulerOptions)
        case option: TaPCTCPOptions =>
          TestingDispatcher.setActorSystem(system)
          //println("Starting with options: " + schedulerOptions + " ----------------------------------------------")
          TestingDispatcher.setUp("TAPCTCP", schedulerOptions)
        case option: POSOptions =>
          TestingDispatcher.setActorSystem(system)
          //println("Starting with options: " + schedulerOptions + " ----------------------------------------------")
          TestingDispatcher.setUp("POS", schedulerOptions)
        case option: DPOSOptions =>
          TestingDispatcher.setActorSystem(system)
          //println("Starting with options: " + schedulerOptions + " ----------------------------------------------")
          TestingDispatcher.setUp("DPOS", schedulerOptions)
        case option: RaposOptions =>
          TestingDispatcher.setActorSystem(system)
          //println("Starting with options: " + schedulerOptions + " ----------------------------------------------")
          TestingDispatcher.setUp("RAPOS", schedulerOptions)
        case option: RandomWalkOptions =>
          TestingDispatcher.setActorSystem(system)
          //println("Starting with options: " + schedulerOptions + " ----------------------------------------------")
          TestingDispatcher.setUp("RANDOM", schedulerOptions)
      }
      TestingDispatcher.awaitTermination()
    }
  }
}

