import scala.collection.immutable.HashMap

package object bench {

  case class BenchMarkOptions(numMsgsToDependentActor: Int, numIndependentActors: Int, chainLengthOfDependentActors: Int, chainLengthOfInDependentActors: Int, seededBug: Int, fileName: String)
}
