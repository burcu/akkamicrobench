package bench

import akka.actor.{Actor, ActorLogging, Props}

class ActorWithDependentMsgs(chainLength: Int, seededBug: Int, fileName: String) extends Actor with ActorLogging {

    import ActorWithDependentMsgs._

    var processedMsgs = List[Int]()

    def receive: PartialFunction[Any, Unit] = {
      case RacyRequest(msgId, length) =>
        //log.info("Received request: " + length)
        processedMsgs = processedMsgs :+ msgId
        if(length-1 > 0) self ! RacyResponse(msgId + 1, length - 1)

      case RacyResponse(msgId, length) =>
        //log.info("Received self-response: " + length)
        processedMsgs = processedMsgs :+ msgId
        if(length-1 > 0) self ! RacyResponse(msgId + 1, length - 1)

      case _ => println("Unrecognized message")
    }

    private def check(depth: Int) = depth match {
      // all seeded bugs requires 3 chains with dependent events

      case 2 => // // strong hitting 2 events, 1 priority change point
        if(processedMsgs.indexOf(chainLength) != -1 &&
          processedMsgs.indexOf(chainLength + 1) != -1 &&
          processedMsgs.indexOf(chainLength + 1) > processedMsgs.indexOf(chainLength) &&

          processedMsgs.indexOf(2 * chainLength) != -1 &&
          processedMsgs.indexOf(2 * chainLength + 1) != -1 &&
          processedMsgs.indexOf(2 * chainLength + 1) > processedMsgs.indexOf(2 * chainLength)
        ) {
          Reporter.write("Detected violation d=2: " + processedMsgs + "\n", fileName)
          context.system.terminate()
        }

      /*case 31 => // strong hitting 3 events, 2 priority change points
        if(
          processedMsgs.indexOf(chainLength) != -1 &&
            processedMsgs.indexOf(chainLength + 1) != -1 &&
            processedMsgs.indexOf(chainLength + 1) > processedMsgs.indexOf(chainLength) &&

            processedMsgs.indexOf(2 * chainLength) != -1 &&
            processedMsgs.indexOf(2 * chainLength + 1) != -1 &&
            processedMsgs.indexOf(chainLength + 1) < processedMsgs.indexOf(2 * chainLength + 1) &&
            processedMsgs.indexOf(2 * chainLength + 1) < processedMsgs.indexOf(2 * chainLength)

        ) {
          Reporter.write("Detected violation d=3-1: " + processedMsgs + "\n", fileName)
          context.system.terminate()
        }
      */
      case 3 => // strong hitting 3 events, 2 priority change points
        if(
          processedMsgs.indexOf(chainLength) != -1 &&
            processedMsgs.indexOf(chainLength + 1) != -1 &&
            processedMsgs.indexOf(chainLength + 1) > processedMsgs.indexOf(chainLength) &&

            processedMsgs.indexOf(2 * chainLength) != -1 &&
            processedMsgs.indexOf(2 * chainLength + Math.ceil(chainLength /2)) != -1 &&
            processedMsgs.indexOf(chainLength + 1) < processedMsgs.indexOf(2 * chainLength + Math.ceil(chainLength /2)) &&
            processedMsgs.indexOf(2 * chainLength + Math.ceil(chainLength /2)) < processedMsgs.indexOf(2 * chainLength)

        ) {
          Reporter.write("Detected violation d=3: " + processedMsgs + "\n", fileName)
          context.system.terminate()
        }
      /*
      case 33 => // strong hitting 3 events, 2 priority change points
        if(
          processedMsgs.indexOf(chainLength) != -1 &&
            processedMsgs.indexOf(chainLength + 1) != -1 &&
            processedMsgs.indexOf(chainLength + 1) > processedMsgs.indexOf(chainLength) &&

            processedMsgs.indexOf(2 * chainLength) != -1 &&
            processedMsgs.indexOf(3 * chainLength) != -1 &&
            processedMsgs.indexOf(chainLength + 1) < processedMsgs.indexOf(3 * chainLength) &&
            processedMsgs.indexOf(3 * chainLength) < processedMsgs.indexOf(2 * chainLength)

        ) {
          Reporter.write("Detected violation d=3-3: " + processedMsgs + "\n", fileName)
          context.system.terminate()
        }
        */

      case _ =>
        println("No such seeded bug is supported. Please try with d=2 or d=3.")
        System.exit(-1)
    }

  override def aroundPostStop(): Unit = check(seededBug)
}

object ActorWithDependentMsgs {
  def props(chainLength: Int, seededBugDepth: Int, fileName: String): Props = Props(new ActorWithDependentMsgs(chainLength, seededBugDepth, fileName))
  case class RacyRequest (msgId: Int, msgCount: Int)
  case class RacyResponse(msgId: Int, msgCount: Int)
}
